﻿using System.Collections.Generic;
using UnityEngine;

namespace AutoRove
{
    /// <summary>
    /// the main addon class to track behavier across multiple vessels
    /// </summary>
    [KSPAddon(KSPAddon.Startup.SpaceCentre, true)]
    class autoRoveMain : MonoBehaviour
    {
        private int frameCounter = 0;
        private int framesPerUpdate = 0;
        private autoRoveGUI.AppButtonWindow window;
        private List<string[]> items = new List<string[]>();
        private GameScenes currentScene = HighLogic.LoadedScene;

        private void toggleAppLauncher()
        {
            if (window == null)
            {
                window = gameObject.AddComponent<AutoRove.autoRoveGUI.AppButtonWindow>();
                window.items = items;
            }
            else if (window != null)
            {
                window.close();
            }
        }

        void Awake()
        {
            Texture icon = GameDatabase.Instance.GetTexture("AutoRove/appLauncherButton", false);
            ApplicationLauncher.AppScenes visibility = ApplicationLauncher.AppScenes.MAPVIEW | ApplicationLauncher.AppScenes.SPACECENTER | ApplicationLauncher.AppScenes.TRACKSTATION | ApplicationLauncher.AppScenes.FLIGHT;

            // registering the button
            if (ApplicationLauncher.Ready)
            {
                Debug.Log(this.name + ": registering AppButton!");
                ApplicationLauncherButton appButton = ApplicationLauncher.Instance.AddModApplication(toggleAppLauncher, toggleAppLauncher, null, null, null, null, visibility, icon);
            }
        }

        void Start()
        {
            DontDestroyOnLoad(this);
            framesPerUpdate = autoRoveConfig.framesPerUpdate;
        }

        void Update()
        {
            frameCounter += 1;
            // update code goes here
            if (frameCounter == framesPerUpdate)
            {
                if (items != null)
                {
                    items.Clear();
                }
                foreach (Vessel ship in FlightGlobals.Vessels)
                {
                    if (!ship.loaded && !ship.isActiveVessel && ship.situation == Vessel.Situations.LANDED)
                    {
                        //autoRoveUtils.debugMessage("found a rover!");
                        Rover rover = new Rover().Initialize(ship);
                        if (rover != null)
                        {
                            //autoRoveUtils.debugMessage("Rover is not null!");
                            //if (rover.move()) { autoRoveUtils.debugMessage("moved rover!"); }
                            rover.move();

                            // updating the appLauncher window
                            string distance = (rover.distanceToTarget / 1000).ToString("F1") + " km";
                            string[] item = new string[4] { rover.name, rover.body.name, rover.target, distance };
                            items.Add(item);
                        }
                    }
                }
                frameCounter = 0;
            }

            // close the appLauncher window on scene change
            if (currentScene != HighLogic.LoadedScene)
            {
                if (window != null) { window.close(); }
                currentScene = HighLogic.LoadedScene;
            }
        }
    }
}
